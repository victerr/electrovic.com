<?php

$router->get('', 'ArticuloController@listar');

$router->get('articulos', 'ArticuloController@listar');
$router->get('articulos/nuevo', 'ArticuloController@nuevoArticulo', 'ROLE_USER');
$router->post('articulos/enviar', 'ArticuloController@nuevo', 'ROLE_USER');
$router->delete('articulos/:id', 'ArticuloController@eliminar', 'ROLE_USER');
$router->get('articulos/:id', 'ArticuloController@show'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('articulos/:id/editar', 'ArticuloController@editar', 'ROLE_USER');
$router->get('articulos/:id/foto', 'ArticuloController@getFoto'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('articulos/:id/miniatura', 'ArticuloController@getMiniatura');//No le pongo rango para que se vean cuando no estás logeado.
$router->post('articulos/:id/actualizar', 'ArticuloController@actualizar', 'ROLE_USER');

$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');

$router->get('registro', 'PagesController@registro');

$router->get('usuarios', 'UsuarioController@listar', 'ROLE_ADMIN');
$router->post('usuarios/nuevo', 'UsuarioController@nuevo');
$router->get('usuarios/:id', 'UsuarioController@verPerfil', 'ROLE_USER');
$router->get('usuarios/:id/editar', 'UsuarioController@editar', 'ROLE_USER');
$router->post('usuarios/:id/actualizar', 'UsuarioController@actualizar', 'ROLE_USER');
$router->get('usuarios/:id/articulos', 'UsuarioController@verArticulos', 'ROLE_USER');
$router->get('usuarios/:id/foto', 'UsuarioController@getFoto'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('articulos/:id/miniatura', 'UsuarioController@getMiniatura');

