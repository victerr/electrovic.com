<?php

namespace EV\app\controllers;

use EV\app\entity\Articulo;
use EV\app\repository\ArticuloRepository;
use EV\app\utils\File;
use EV\core\App;
use EV\core\exceptions\ValidationException;
use EV\core\helpers\FlashMessage;
use EV\core\helpers\MyLogger;
use EV\core\Response;

class ArticuloController
{
    /**
     * @return ArticuloRepository
     */
    private function getContactoRepository() : ArticuloRepository
    {
        return App::getRepository(ArticuloRepository::class);
    }

    public function show(int $id)
    {
        /** @var Articulo $articulo */
        $articulo = ArticuloRepository::getRepository()->find($id);

        if(App::get('usuario'))
            $usuario = App::get('usuario');
        else
            $usuario=null;

        Response::renderView('ver-articulo', [
            'usuario'=> $usuario,
            'articulo' => $articulo
        ]);
    }

    public function nuevoArticulo(){

        Response::renderView('nuevo-articulo');
    }


    private function generaImagenFoto(string $foto)
    {
        if (is_file($foto) === true)
            $imagen = imagecreatefrompng($foto);
        else
        {
            $alto = 200;
            $ancho = 200;
            $imagen = imagecreatetruecolor($ancho, $alto);
            $blanco = imagecolorallocate($imagen, 255, 255, 255);
            $azul = imagecolorallocate($imagen, 0, 0, 64);
            imagefill($imagen, 0, 0, $azul);
            imagestring($imagen, 4, 50, 150, 'SIN FOTO', $blanco);
        }

        header("Content-Type: image/png");
        imagepng($imagen);
        imagedestroy($imagen);
    }

    public function getFoto(int $id)
    {
        /** @var Articulo $articulo */
        $articulo = ArticuloRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $articulo->getFoto();

        $this->generaImagenFoto($foto);
    }

    public function getMiniatura(int $id)
    {
        /** @var Articulo $articulo */
        $articulo = ArticuloRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $articulo->getMiniatura();

        $this->generaImagenFoto($foto);
    }

    /**
     * @throws \EV\core\exceptions\QueryException
     */
    public function listar()
    {
        $articuloRepository = ArticuloRepository::getRepository();
        /*$articulos = $articuloRepository->findBy(
            ['usuario' => App::get('usuario')->getId()]
        );*/
        $articulos=$articuloRepository->findAll();

        if(App::get('usuario'))
            $usuarioId = App::get('usuario')->getId();
        else
            $usuarioId=null;

        $mensaje = FlashMessage::get('mensaje');
        $error = FlashMessage::get('error');
        $numArticulos = count($articulos);

        Response::renderView('articulos', [
            'articulos' => $articulos,
            'articuloRepository' => $articuloRepository,
            'usuarioId' => $usuarioId,
            'mensaje' => $mensaje,
            'error' => $error
        ]);
    }

    /**
     * @param string $messsage
     * @return ValidationException
     */
    private function getValidationException(string $messsage): ValidationException
    {
        $validationException = new ValidationException($messsage);
        $validationException->setRedireccion('articulos');
        return $validationException;
    }

    private function creaFoto(Articulo & $articulo)
    {
        $file = new File(
            'foto',
            'uploads/articulos/',
            ['image/png']
        );

        $file->uploadFile();

        $foto = $file->getFileUrl();

        $articulo->setFoto($foto);
    }

    /**
     * @throws ValidationException
     * @throws \EV\core\exceptions\AppException
     */
    public function nuevo()
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw $this->getValidationException('El campo nombre no se puede quedar vacío');
        if(!isset($_POST['descripcion']) || empty($_POST['descripcion']))
            throw $this->getValidationException('El campo descripcion no se puede quedar vacío');


        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
        $categoria = $_POST['categoria'];
        $estado = $_POST['estado'];
        $precio = $_POST['precio'];

        $articulo = new Articulo();
        $articulo->setNombre($nombre);
        $articulo->setDescripcion($descripcion);
        $articulo->setCategoria($categoria);
        $articulo->setEstado($estado);
        $articulo->setPrecio($precio);
        $articulo->setUsuario(App::get('usuario')->getId());

        $this->creaFoto($articulo);

        $this->getContactoRepository()->nuevo($articulo);

        $mensaje = "Se ha insertado correctamente el articulo con nombre $nombre";
        App::getService(MyLogger::class)->addMessage($mensaje);

        FlashMessage::set('mensaje', $mensaje);

        App::get('router')->redirect('articulos');
    }

    /**
     * @throws \EV\core\exceptions\QueryException
     * @throws \EV\core\exceptions\AppException
     */
    public function editar(int $id)
    {
        $articuloRepository = ArticuloRepository::getRepository();
        $articulo = $articuloRepository->find($id);

        if(App::get('usuario'))
            $usuarioId = App::get('usuario')->getId();
        else
            $usuarioId=null;

        Response::renderView('ver-articulo', [
            'id' => $id,
            'usuarioId'=> $usuarioId,
            'articulo' => $articulo,
            'articuloRepository' => $articuloRepository
        ]);
    }

    /**
     * @param int $id
     * @throws ValidationException
     * @throws \EV\core\exceptions\AppException
     * @throws \EV\core\exceptions\NotFoundException
     * @throws \EV\core\exceptions\QueryException
     */
    public function actualizar(int $id)
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');
        if (!isset($_POST['descripcion']) || empty($_POST['descripcion']))
            throw new ValidationException('El campo descripcion no se puede quedar vacío');
        if (!isset($_POST['categoria']) || empty($_POST['categoria']))
            throw new ValidationException('El campo categoría no se puede quedar vacío');
        if (!isset($_POST['estado']) || empty($_POST['estado']))
            throw new ValidationException('El campo estado no se puede quedar vacío');
        if (!isset($_POST['precio']) || empty($_POST['precio']))
            throw new ValidationException('El campo precio no se puede quedar vacío');

        //AÑADIR COMPROBACIONES DE CAMPOS RELLENOS

        /** @var Articulo $articulo */
        $articulo = ArticuloRepository::getRepository()->find($id);


        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
        $categoria = $_POST['categoria'];
        $estado = $_POST['estado'];
        $precio = $_POST['precio'];


        $articulo->setNombre($nombre);
        $articulo->setDescripcion($descripcion);
        $articulo->setCategoria($categoria);
        $articulo->setEstado($estado);
        $articulo->setPrecio($precio);


        ArticuloRepository::getRepository()->edita($articulo);

        FlashMessage::set('mensaje', 'El articulo se ha editado correctamente');

        App::get('router')->redirect('articulos');
    }

    /**
     * @throws \EV\core\exceptions\AppException
     * @throws \EV\core\exceptions\NotFoundException
     * @throws \EV\core\exceptions\QueryException
     */
    public function eliminar(int $id)
    {
        $articuloRepository = $this->getContactoRepository();

         $articulo = $articuloRepository->find($id);
        /** @var Articulo $articulo */
        $articuloRepository->elimina($articulo);

        header('Content-Type: application/json');

        echo json_encode([ 'ok' => true ]);
    }
}