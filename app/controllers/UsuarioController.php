<?php

namespace EV\app\controllers;

use DateTime;
use EV\app\entity\Articulo;
use EV\app\entity\Usuario;
use EV\app\repository\ArticuloRepository;
use EV\app\repository\UsuarioRepository;
use EV\app\utils\File;
use EV\core\App;
use EV\core\exceptions\ValidationException;
use EV\core\helpers\FlashMessage;
use EV\core\helpers\MyLogger;
use EV\core\Response;
use EV\core\Security;

class UsuarioController
{
    public function listar()
    {
        $usuarios = App::getRepository(UsuarioRepository::class)->findAll();

        Response::renderView(
            'usuarios',
            [
                'usuarios' => $usuarios
            ]
        );
    }

    public function verPerfil(int $id)
    {
        /** @var Usuario $usuario */
        $usuario = UsuarioRepository::getRepository()->find($id);

        Response::renderView('ver-perfil', [ 'usuario' => $usuario]);
    }

    public function verArticulos()
    {
        $articuloRepository = ArticuloRepository::getRepository();
        $articulos = $articuloRepository->findBy(
            ['usuario' => App::get('usuario')->getId()]
        );
        //$articulos=$articuloRepository->findAll();

        if(App::get('usuario'))
            $usuarioId = App::get('usuario')->getId();
        else
            $usuarioId=null;

        $mensaje = FlashMessage::get('mensaje');
        $error = FlashMessage::get('error');
        $numArticulos = count($articulos);

        Response::renderView('articulos', [
            'articulos' => $articulos,
            'articuloRepository' => $articuloRepository,
            'usuarioId' => $usuarioId,
            'mensaje' => $mensaje,
            'error' => $error
        ]);
    }

    public function editar(int $id)
    {
        $usuario = App::getRepository(UsuarioRepository::class)->findBy($id);
        $usuarioRepository = UsuarioRepository::getRepository();

        Response::renderView('usuarios', [
            'id' => $id,
            'usuario' => $usuario,
            'usuarioRepository' => $usuarioRepository
        ]);
    }

    public function actualizar(int $id)
    {
        $usuario = UsuarioRepository::getRepository()->find($id);

        if (isset($_POST['password']) && !empty($_POST['password'])){
            if (!isset($_POST['password2']) || empty($_POST['password2']))
                throw new ValidationException('Vuelve a introducir la contraseña');
            else{
                $password = $_POST['password'];
                $password2 = $_POST['password2'];

                if($password != $password2)
                    throw new ValidationException('Las contraseñas no coinciden');
                else{
                    $usuario->setPassword(Security::encrypt($password));
                }

            }
        }

        if (isset($_FILES['avatar'])){
            $avatar = $_POST['avatar'];
            $this->creaFoto($usuario);
        }

        UsuarioRepository::getRepository()->edita($usuario);

        FlashMessage::set('mensaje', 'La información del perfil se ha actualizado con éxito.');

        App::get('router')->redirect('articulos');
    }

    private function creaFoto(Usuario & $usuario)
    {
        $file = new File(
            'avatar',
            'uploads/avatares/',
            ['image/png']
        );

        $file->uploadFile();

        $avatar = $file->getFileUrl();

        $usuario->setAvatar($avatar);
    }


    public function nuevo()
    {
        if (!isset($_POST['username']) || empty($_POST['username']))
            throw new ValidationException('El campo username no se puede quedar vacío');

        $username = $_POST['username'];

        if (!isset($_POST['email']) || empty($_POST['email']))
            throw new ValidationException('El campo email no se puede quedar vacío');

        $email = $_POST['email'];

        if (!isset($_POST['password']) || empty($_POST['password']))
            throw new ValidationException('El campo password no se puede quedar vacío');

        $password = $_POST['password'];

        if (!isset($_POST['password2']) || empty($_POST['password']))
            throw new ValidationException('Debes volver a introducir la contraseña');

        if ($_POST['password'] !== $_POST['password2'])
            throw new ValidationException('Las contraseñas no coinciden');

        $fecha_nac=$_POST['fecha_nac'];

        $usuario = new Usuario();
        $usuario->setUsername($username);
        $usuario->setEmail($email);
        $usuario->setPassword(Security::encrypt($password));
        /*if (!isset($_POST['fecha_nac']))
            $usuario->setFechaNac(DateTime::createFromFormat('d/m/Y H:i:s', $fecha_nac));*/
        $usuario->setRango('NOOB');//TODO: CAMBIARLO PARA COGER EL RANGO MÁS BAJO DE UN ARRAY PREDEFINIDO
        $usuario->setRole('ROLE_USER');
        $this->creaFoto($usuario);

        UsuarioRepository::getRepository()->save($usuario);

        $mensaje = "El usuario se ha insertado correctamente";

        App::getService(MyLogger::class)->addMessage($mensaje);

        FlashMessage::set('mensaje', $mensaje);


        App::get('router')->redirect('login');
    }

    private function generaImagenFoto(string $foto)
    {
        if (is_file($foto) === true)
            $imagen = imagecreatefrompng($foto);
        else
        {
            $alto = 200;
            $ancho = 200;
            $imagen = imagecreatetruecolor($ancho, $alto);
            $blanco = imagecolorallocate($imagen, 255, 255, 255);
            $azul = imagecolorallocate($imagen, 0, 0, 64);
            imagefill($imagen, 0, 0, $azul);
            imagestring($imagen, 4, 50, 150, 'SIN FOTO', $blanco);
        }

        header("Content-Type: image/png");
        imagepng($imagen);
        imagedestroy($imagen);
    }

    public function getFoto(int $id)
    {
        /** @var Articulo $articulo */
        $usuario = UsuarioRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $usuario->getAvatar();

        $this->generaImagenFoto($foto);
    }

    public function getMiniatura(int $id)
    {
        /** @var Articulo $articulo */
        $usuario = UsuarioRepository::getRepository()->find($id);

        $foto = __DIR__ . '/../../' . $usuario->getMiniatura();

        $this->generaImagenFoto($foto);
    }
}