<?php

namespace EV\app\repository;

use EV\core\App;
use EV\core\database\QueryBuilder;
use EV\app\entity\Grupo;

class GrupoRepository extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('grupo', Grupo::class,  $loadDataBeforeConstruct=false);
    }

    public function elimina(Grupo $grupo)
    {
        $this->remove($grupo);
    }

    /**
     * @return GrupoRepository
     */
    public static function getRepository() : GrupoRepository
    {
        return App::getRepository(GrupoRepository::class);
    }
}