<?php

namespace EV\app\utils;

use EV\core\exceptions\FileException;

class File
{
    const DIMENSION_X_MINIATURA = 100;
    const DIMENSION_Y_MINIATURA = 100;
    /**
     * @var string []
     */
    private $tiposPermitidos;
    /**
     * @var string
     */
    private $uploadDir;
    /**
     * @var string
     */
    private $fileName;
    /**
     * @var array
     */
    private $file;
    /** @var bool */
    private $miniatura;

    /**
     * File constructor.
     * @param string $fieldName
     * @throws FileException
     */
    public function __construct(
        string $fieldName, string $uploadDir, array $tiposPermitidos, $miniatura=true)
    {
        if (!isset($_FILES[$fieldName]))
            throw new FileException('No se ha recibido el campo ' . $fieldName);

        $this->file = $_FILES[$fieldName];
        $this->fileName = $this->file['name'];
        $this->uploadDir = $uploadDir;
        if ($this->uploadDir[strlen($this->uploadDir)-1] !== '/')
            $this->uploadDir .= '/';
        $this->tiposPermitidos = $tiposPermitidos;
        $this->miniatura = $miniatura;
    }

    /**
     * @throws FileException
     */
    private function checkUploadError()
    {
        if ($this->file['error'] !== UPLOAD_ERR_OK)
        {
            switch($this->file['error'])
            {
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException('El fichero es demasiado grande');

                default:
                    throw new FileException('El fichero no se ha subido correctamente');
            }
        }
    }

    /**
     * @throws FileException
     */
    private function checkFileType() : void
    {
        if (!in_array($this->file['type'], $this->tiposPermitidos))
            throw new FileException('Tipo de archivo no permitido');
    }

    /**
     * @throws FileException
     */
    private function checkUploadFile() : void
    {
        if (is_uploaded_file($this->file['tmp_name']) === false)
            throw new FileException('Posible ataque');
    }

    private function buildFileName() : void
    {
        $this->fileName = $this->uploadDir . $this->file['name'];
        if (is_file($this->fileName) === true)
        {
            $idUnico = time();
            $this->fileName = $this->uploadDir . $idUnico . $this->file['name'];
        }
    }

    private function getFilePath()
    {
        return __DIR__ . '/../../' . $this->fileName;
    }

    private function getMiniaturaPath()
    {
        $filePath = $this->getFilePath();
        $posBarra = strripos($filePath, '/');
        $miniaturaPath = substr($filePath, 0, $posBarra);
        $miniaturaPath .= '/miniaturas';
        $miniaturaPath .= substr($filePath, $posBarra);

        return $miniaturaPath;
    }

    /**
     * @throws FileException
     */
    private function moveFile() : void
    {
        $this->buildFileName();

        if (move_uploaded_file($this->file['tmp_name'], $this->getFilePath()) === false)
            throw new FileException('No se ha podido mover el archivo');
    }

    private function creaMiniatura()
    {
        $filePath = $this->getFilePath();
        $miniaturaPath = $this->getMiniaturaPath();

        $image = imagecreatefrompng($filePath);
        $imageMiniatura = imagecreatetruecolor(
            self::DIMENSION_X_MINIATURA, self::DIMENSION_Y_MINIATURA);

        imagecopyresampled(
            $imageMiniatura, $image,
            0, 0,
            0, 0,
            self::DIMENSION_X_MINIATURA, self::DIMENSION_Y_MINIATURA,
            imagesx($image), imagesy($image));

        imagepng($imageMiniatura, $miniaturaPath, 0, PNG_NO_FILTER);

        imagedestroy($image);
        imagedestroy($imageMiniatura);
    }

    /**
     * @throws FileException
     */
    public function uploadFile() : void
    {
        $this->checkUploadError();

        $this->checkFileType();

        $this->checkUploadFile();

        $this->moveFile();

        if($this->miniatura === true)
            $this->creaMiniatura();
    }

    public function getFileUrl()
    {
        return $this->fileName;
    }
}