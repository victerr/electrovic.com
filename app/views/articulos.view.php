<div class="row">
    <?php if (isset($id)) :?>
    <form action="/articulos/<?= $id ?>/actualizar" method="post">
        <?php endif; ?>
        <table class="table">
            <br>
            <?php foreach (array_reverse($articulos) as $articulo ) : ?>
                <e>
                    <div class="card col-md-4">
                        <img src="/articulos/<?= $articulo->getId() ?>/miniatura" class="card-img-top" alt="<?= $articulo->getFoto() ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $articulo->getNombre() ?></h5>
                            <p class="card-text"><?= $articulo->getDescripcion() ?></p>
                            <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-primary">Ver</a> &nbsp;
                            <?php if ($_usuario) :?>
                                <?php if ($_usuario->getId() == $articulo->getUsuario()) :?>
                                    <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-danger"><i class="patata">Eliminar</i></a>
                                    <a href="/articulos/<?= $articulo->getId() ?>/editar" class="btn btn-primary">Modificar</a>
                                <?php endif; ?>
                                <?php if ($_usuario->getId() != $articulo->getUsuario()) :?>
                                    <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-primary">Comprar</a>
                                    <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-primary">Al carro</a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </e>
            <?php endforeach; ?>

        </table>
        <?php if (isset($id)) :?>
    </form>
<?php endif; ?>
</div>
<script src="/js/articulos.js"></script>
