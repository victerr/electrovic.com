<br>
<form method="post" action="/articulos/enviar" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre">
        </div>
    </div>
    <div class="form-group col-md-2"">
        <label for="precio">Precio</label>
        <input type="number" class="form-control" name="precio">
    </div>
    <div class="form-group col-md-6">
        <label for="descripcion">Descripcion</label>
        <textarea class="form-control" name="descripcion" rows="3"></textarea>
    </div>
    <div class="form-group col-md-4"">
        <label for="categoria">Categoria</label>
        <input type="text" class="form-control" name="categoria">
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" name="estado">
        </div>
    </div>
    <div class="form-group col-md-6">
        <label for="foto">Imagen</label>
        <input type="file" class="form-control-file" name="foto">
    </div>
    <button type="submit" class="btn btn-success col-md-5">Enviar</button>
    </div>

</form>