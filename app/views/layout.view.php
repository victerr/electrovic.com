<?php include __DIR__ . '/partials/cabecera.part.php'; ?>
<div class="container">
    <?php include __DIR__ . '/partials/mensaje.part.php'; ?>
    <?php include __DIR__ . '/partials/error.part.php'; ?>
    <?= $mainContent ?>
</div>
<?php include __DIR__ . '/partials/pie.part.php'; ?>
