
<br>
<form method="post" action="/usuarios/nuevo" enctype="multipart/form-data">
    <div class="col col-md-5">
        <div class="form-group">
            <label for="username">Nombre de usuario</label>
            <input type="text" class="form-control" name="username">
        </div>

        <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
            <label for="fecha_nac">Fecha de nacimiento</label>
            <input type="date" class="form-control" name="fecha_nac">
        </div>
        <div class="form-group col-md-6">
            <label for="avatar">Imagen</label>
            <input type="file" class="form-control-file" name="avatar">
        </div>

    </div>
    <div class="col col-md-5">
        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="password2">Repite la contraseña:</label>
            <input type="password" class="form-control" name="password2">
            <br>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-success col-md-5">Enviar</button>
    </div>
</form>
