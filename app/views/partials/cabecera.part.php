<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Electro - HTML Ecommerce Template</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css"/>
    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="/css/slick.css"/>
    <link type="text/css" rel="stylesheet" href="/css/slick-theme.css"/>

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="/css/nouislider.min.css"/>

    <!-- Font Awesome Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="/css/cards.css"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- HEADER -->
<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-left">
                <li>
                    <a href="#"><i class="fa fa-phone"></i> Teléfono</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-map-marker"></i> Dirección</a>
                </li>
            </ul>
            <ul class="header-links pull-right">
                <?php if(!is_null($_usuario)) : ?>
                    <?php if($_usuario->getRole()=='ROLE_ADMIN') : ?>
                        <li>
                            <a href="/usuarios"><i class="fa fa-users"></i> Gestionar usuarios</a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="#"><i class="fa fa-envelope"></i> Mis mensajes</a>
                    </li>
                    <li>
                        <a href="/usuarios/<?= $_usuario->getId()?>/articulos"><i class="fas fa-book"></i> Mis artículos</a>
                    </li>
                    <li>
                        <a href="/usuarios/<?= $_usuario->getId()?>"><i class="fas fa-user"></i> Mi perfil</a>
                    </li>
                    <li>
                        <a href="/logout"> <i class="fas fa-sign-out-alt"></i> <?= $_usuario->getUsername()?> - Salir</a>
                    </li>
                <?php else : ?>
                    <li>
                        <a href="/login"> <i class="fas fa-sign-out-alt"></i> Entrar</a>
                    </li>

                    <li>
                        <a href="/registro"> <i class="fas fa-sign-out-alt"></i> Registrarse</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->

    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="d-flex flex-row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="/" class="logo">
                            <img src="/img/logo.png" alt="Logo">
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-6">
                    <div class="header-search">
                        <form>
                            <select class="input-select">
                                <option value="0">All Categories</option>
                                <option value="1">Category 01</option>
                                <option value="1">Category 02</option>
                            </select>
                            <input class="input" placeholder="Búsqueda...">
                            <button class="search-btn">Buscar</button>
                        </form>
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="d-flex flex-col">
                    <div class="header-ctn">
                        <!-- Wishlist -->
                        <?php if(!is_null($_usuario)) : ?>
                        <div>
                            <a href="/articulos/nuevo"><i class="fas fa-plus-square"></i> <span>Añadir un artículo</span></a>
                        </div>
                        <!-- /Wishlist -->
                        <!-- Cart -->
                        <div>
                            <a href="#"><i class="fa fa-shopping-cart"></i><span>Mi carro de la compra</span></a>
                        </div>
                        <?php endif;?>
                        <!-- /Cart -->

                        <!-- Menu Toogle -->
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- /MAIN HEADER -->
</header>
<!-- /HEADER -->

<!-- NAVIGATION -->
<nav id="navigation">
    <!-- container -->
    <div class="container">
        <!-- responsive-nav -->
        <div id="responsive-nav">
            <!-- NAV -->
            <ul class="main-nav nav navbar-nav">
                <li class="active">
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">PC Sobremesa</a>
                </li>
                <li>
                    <a href="#">Portátiles</a>
                </li>
                <li>
                    <a href="#">Móviles</a>
                </li>
                <li>
                    <a href="#">Consolas</a>
                </li>
            </ul>
            <!-- /NAV -->
        </div>
        <!-- /responsive-nav -->
    </div>
    <!-- /container -->
</nav>
<!-- /NAVIGATION -->
<body>