<div class="row">
    <form method="post" action="/usuarios/nuevo" enctype="multipart/form-data">
        <label for="username">Username:</label>
        <input type="text" name="username">
        <label for="password">Password:</label>
        <input type="password" name="password">
        <input type="submit" value="Enviar" name="enviar">
    </form>
</div>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Operaciones</th>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Role</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($usuarios as $usuario ) : ?>
            <tr>
                <td>
                    <div class="btn-group" role="group" aria-label="Operaciones">
                        <a href="#" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                        <a href="/usuarios/<?= $usuario->getId() ?>/eliminar" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                        <a href="/usuarios/<?= $usuario->getId() ?>" class="btn btn-secondary"><i class="fa fa-eye"></i></a>
                        <a href="/usuarios/<?= $usuario->getId() ?>" class="btn btn-secondary"><i class="fa fa-hammer"></i></a>
                    </div>
                </td>
                <td><?= $usuario->getId() ?></td>
                <td><?= $usuario->getUsername() ?></td>
                <td><?= $usuario->getRole() ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
