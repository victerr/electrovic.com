<div class="row">
    <form method="post" action="/grupos/nuevo" enctype="multipart/form-data">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre">
        <input type="submit" value="Enviar" name="enviar">
    </form>
</div>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Operaciones</th>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Núm. articulos</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($grupos as $grupo ) : ?>
            <tr>
                <td>
                    <div class="btn-group" role="group" aria-label="Operaciones">
                        <a href="#" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                        <a href="/grupos/<?= $grupo->getId() ?>/eliminar" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
                <td><?= $grupo->getId() ?></td>
                <td><?= $grupo->getNombre() ?></td>
                <td><?= $grupo->getNumArticulos() ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
