<?php

use EV\app\repository\UsuarioRepository;
use EV\core\App;
use EV\core\exceptions\AccessDeniedException;
use EV\core\exceptions\MyException;
use EV\core\exceptions\NotFoundException;
use EV\core\exceptions\QueryException;
use EV\core\helpers\FlashMessage;
use EV\core\Router;
use EV\core\Request;

try
{
    require '../vendor/autoload.php';
    require '../core/bootstrap.php';

    $router = Router::load(__DIR__ . '/../app/routes.php');
    App::bind('router', $router);

    if (isset($_SESSION['usuario']))
    {
        $usuario = App::getRepository(UsuarioRepository::class)
            ->find($_SESSION['usuario']);
    }
    else
        $usuario = null;

    App::bind('usuario', $usuario);

    $router->direct(Request::uri(), Request::method());
}catch(QueryException $queryException) {
    App::get('router')->errorPage($queryException->getMessage(), 500);
}catch(AccessDeniedException $adException) {
    App::get('router')->errorPage($adException->getMessage(), 403);
}catch(NotFoundException $nfException) {
    App::get('router')->errorPage($nfException->getMessage(), 404);
}catch(MyException $myException) {
    FlashMessage::set('error', $myException->getMessage());

    $redireccion = $myException->getRedireccion();
    if (isset($redireccion) &&  $redireccion !== '')
        App::get('router')->redirect($redireccion);
}
