<?php

namespace EV\core\helpers;

use EV\core\App;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLogger
{
    /**
     * @var \Monolog\Logger
     */
    private $log;

    /**
     * MyLogger constructor.
     * @throws \EV\core\exceptions\AppException
     * @throws \Exception
     */
    public function __construct()
    {
        $config = App::get('config')['logger'];

        $this->log = new Logger('log-ev');
        $this->log->pushHandler(
            new StreamHandler($config['filename'], $config['log_level'])
        );
    }

    /**
     * @param string $message
     */
    public function addMessage(string $message)
    {
        $this->log->info($message);
    }
}