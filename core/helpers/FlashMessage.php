<?php

namespace EV\core\helpers;

class FlashMessage
{
    public static function set(string $name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public static function get(string $name)
    {
        if (isset($_SESSION[$name]))
        {
            $value = $_SESSION[$name];
            unset($_SESSION[$name]);
        }
        else
            $value = null;

        return $value;
    }
}